import 'package:intl/intl.dart';

class Calculator {
  static final Calculator _instance = Calculator._();
  double result;
  String expression;

  String errorMessage;

  bool hasError;

  Calculator._() {
    result = 0;
    expression = "";
    errorMessage = "";
    hasError = false;
  }

  factory Calculator() => _instance;

  String getResult() {
    String formattedResult = NumberFormat("#,###").format(result.truncate());
    String decimals = result.toString().split(".")[1];
    if (int.parse(decimals) != 0) {
      return "$formattedResult.$decimals";
    } else {
      return formattedResult;
    }
  }

  void reset() {
    result = 0;
    expression = "";
    errorMessage = "";
    hasError = false;
  }

  void switchSign() {
    if (result != 0) {
      result *= -1;

      expression = result.toString();
    }
  }
}
