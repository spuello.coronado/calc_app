import 'package:calc_app/bloc/bloc.dart';
import 'package:calc_app/bloc/calculator_bloc.dart';
import 'package:calc_app/core/symbol_type.dart';
import 'package:calc_app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NumberButton extends StatelessWidget {
  NumberButton({this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      textColor: kCalcAppSecondaryColor,
      onPressed: () {
        BlocProvider.of<CalculatorBloc>(context).add(
            UpdateExpression(symbolType: SymbolType.NUMBER, symbol: title));
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: Text(
        title,
        style: TextStyle(fontSize: 20.0),
      ),
    );
  }
}
