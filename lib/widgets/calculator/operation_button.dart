import 'package:calc_app/bloc/bloc.dart';
import 'package:calc_app/core/symbol_type.dart';
import 'package:calc_app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OperationButton extends StatelessWidget {
  OperationButton({this.symbol, this.symbolType});

  final String symbol;
  final SymbolType symbolType;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: kCalcAppPrimaryColor,
      textColor: kCalcAppLightColor,
      onPressed: () {
        BlocProvider.of<CalculatorBloc>(context).add(UpdateExpression(
            symbolType: symbolType, symbol: symbol.toLowerCase()));
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: Text(
        symbol,
        style: TextStyle(fontSize: 20.0),
      ),
    );
  }
}
