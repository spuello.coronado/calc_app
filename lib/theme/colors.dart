import 'package:flutter/material.dart';

const Color kCalcAppLightColor = Color(0xffF9FBFB);
const Color kCalcAppSecondaryColor = Color(0xff0E5C68);
const Color kCalcAppPrimaryColor = Color(0xff9FBEC3);
