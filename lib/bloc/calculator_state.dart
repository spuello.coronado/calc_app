abstract class CalculatorState {}

class AppStarted extends CalculatorState {}

class ResultLoad extends CalculatorState {}
