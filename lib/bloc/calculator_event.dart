import 'package:calc_app/core/symbol_type.dart';

abstract class CalculatorEvent {}

class UpdateExpression extends CalculatorEvent {
  final String symbol;
  final SymbolType symbolType;

  UpdateExpression({this.symbol, this.symbolType});
}

class LoadCalculator extends CalculatorEvent {}
