import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:calc_app/core/symbol_type.dart';
import 'package:calc_app/models/calculator.dart';

import './bloc.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  @override
  CalculatorState get initialState => ResultLoad();

  @override
  Stream<CalculatorState> mapEventToState(
    CalculatorEvent event,
  ) async* {
    if (event is UpdateExpression) {
      yield* _mapUpdateExpression(event);
    }
  }

  Stream<CalculatorState> _mapUpdateExpression(UpdateExpression event) async* {
    var symbolType = event.symbolType;

    if (symbolType != SymbolType.PERCENT &&
        symbolType != SymbolType.SWITCH_SIGN &&
        symbolType != SymbolType.EQUALITY &&
        symbolType != SymbolType.CLEAR) {
      _concatSymbol(event);
    }

    if (Calculator().hasError || symbolType == SymbolType.CLEAR) {
      Calculator().reset();
    } else if (symbolType == SymbolType.EQUALITY) {
      Calculator().expression = Calculator().result.toString();
    } else if (symbolType == SymbolType.SWITCH_SIGN) {
      Calculator().switchSign();
    } else if (symbolType == SymbolType.PERCENT) {
      Calculator().expression = Calculator()
          .expression
          .replaceAllMapped(RegExp(r"(\d+\.?\d*)$"), (Match match) {
        return (double.parse(match[0]) / 100).toString();
      });
      _mapCompute();
    } else {
      _mapCompute();
    }

    yield ResultLoad();
  }

  void _concatSymbol(UpdateExpression event) {
    if (!RegExp(r"\D$|(\d+\.\d*?)$").hasMatch(Calculator().expression)) {
      Calculator().expression = "${Calculator().expression}${event.symbol}";
    } else if (!RegExp(r"\D$").hasMatch(Calculator().expression) &&
        event.symbolType != SymbolType.DOT) {
      Calculator().expression = "${Calculator().expression}${event.symbol}";
    } else if (event.symbolType == SymbolType.NUMBER) {
      Calculator().expression = "${Calculator().expression}${event.symbol}";
    }
  }

  void _mapCompute() async {
    RegExp firstOrderOperations = RegExp(r"((\d*\.?\d+(x|/){1})+)\d+");
    String expressionTerms = Calculator().expression.replaceAllMapped(
        firstOrderOperations, (Match m) => _multiplyOrDivide(m[0]).toString());
    Calculator().result = _addNumbers(expressionTerms);
  }

  double _addNumbers(String exp) {
    double result = 0;
    RegExp signedNumber = RegExp(r"((\+|-)?\d*\.?\d+)");
    signedNumber.allMatches(exp).forEach((element) {
      result += double.parse(element.group(0));
    });
    return result;
  }

  double _multiplyOrDivide(String exp) {
    String multiplyOperator = "x";
    String divideOperator = "/";

    double result = _getNextNumber(exp);
    List<String> chars = exp.split('');

    for (int i = 0; i < chars.length; i++) {
      if (chars[i] == multiplyOperator && i < chars.length - 1) {
        result *= _getNextNumber(exp.substring(i + 1));
      } else if (chars[i] == divideOperator && i < chars.length - 1) {
        if (_getNextNumber(exp.substring(i + 1)) != 0) {
          result /= _getNextNumber(exp.substring(i + 1));
        } else {
          Calculator().errorMessage = "No es posible dividir por cero";
          Calculator().hasError = true;
        }
      }
    }
    return result;
  }

  double _getNextNumber(String exp) {
    RegExp nextNumber = RegExp(r"(\d*\.?\d*)");

    Iterable<Match> matches = nextNumber.allMatches(exp);
    Match nextNumberMatched =
        matches.firstWhere((element) => element.group(0).isNotEmpty);

    return double.parse(nextNumberMatched.group(0));
  }
}
