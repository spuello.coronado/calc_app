import 'package:calc_app/bloc/bloc.dart';
import 'package:calc_app/screens/CalculatorScreen.dart';
import 'package:calc_app/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/calculator_bloc.dart';

void main() {
  runApp(
    BlocProvider(
      create: (BuildContext context) => CalculatorBloc()
        ..add(
          LoadCalculator(),
        ),
      child: CalcApp(),
    ),
  );
}

class CalcApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Calculator',
      theme: calcAppTheme(),
      home: CalculatorScreen(),
    );
  }

  ThemeData calcAppTheme() {
    ThemeData base = ThemeData.light();
    return base.copyWith(
        scaffoldBackgroundColor: kCalcAppPrimaryColor,
        primaryColor: kCalcAppPrimaryColor,
        accentColor: kCalcAppSecondaryColor,
        primaryTextTheme: base.primaryTextTheme.copyWith());
  }
}
