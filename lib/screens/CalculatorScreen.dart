import 'dart:ui';

import 'package:calc_app/bloc/bloc.dart';
import 'package:calc_app/core/symbol_type.dart';
import 'package:calc_app/models/calculator.dart';
import 'package:calc_app/theme/colors.dart';
import 'package:calc_app/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../theme/colors.dart';

class CalculatorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(
              right: 30.0,
              bottom: 20.0,
            ),
            child: BlocBuilder<CalculatorBloc, CalculatorState>(
                builder: (context, state) {
              if (state is ResultLoad) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: Text(
                        Calculator().expression == null
                            ? ""
                            : Calculator().expression,
                        style: Theme.of(context).textTheme.headline6.copyWith(
                            fontWeight: FontWeight.w400,
                            color: kCalcAppSecondaryColor),
                      ),
                    ),
                    Text(
                      "= ${Calculator().hasError ? Calculator().errorMessage : Calculator().getResult()}",
                      style: Theme.of(context).textTheme.headline4.copyWith(
                          fontWeight: FontWeight.w400,
                          color: kCalcAppSecondaryColor),
                    ),
                  ],
                );
              } else {
                return Center(
                    child: Text(
                        "HA OCURRIDO UN ERROR, POR FAVOR REVISAR SU APLICACION"));
              }
            }),
          ),
        ),
        Expanded(
          child: Container(
              padding: EdgeInsets.only(left: 20.0, top: 20.0),
              decoration: BoxDecoration(
                  color: kCalcAppLightColor,
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [kCalcAppLightColor, kCalcAppPrimaryColor]),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0))),
              child: Table(
                children: [
                  TableRow(children: [
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: "C",
                        symbolType: SymbolType.CLEAR,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: "+/-",
                        symbolType: SymbolType.SWITCH_SIGN,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: "%",
                        symbolType: SymbolType.PERCENT,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: "/",
                        symbolType: SymbolType.DIVIDE,
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "7",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "8",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "9",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: "X",
                        symbolType: SymbolType.MULTIPLY,
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "4",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "5",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "6",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: "-",
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "1",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "2",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "3",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: "+",
                      ),
                    ),
                  ]),
                  TableRow(children: [
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: NumberButton(
                        title: "0",
                      ),
                    ),
                    SizedBox(),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: OperationButton(
                        symbol: ".",
                        symbolType: SymbolType.DOT,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: RaisedButton(
                        color: kCalcAppSecondaryColor,
                        textColor: kCalcAppLightColor,
                        onPressed: () {
                          BlocProvider.of<CalculatorBloc>(context).add(
                              UpdateExpression(
                                  symbolType: SymbolType.EQUALITY,
                                  symbol: "="));
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          "=",
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                    ),
                  ]),
                ],
              )),
        ),
      ],
    ));
  }
}
